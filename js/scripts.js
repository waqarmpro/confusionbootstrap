$(document).ready(function() {
    $("#mycarousel").carousel({interval: 2000});
    $("#carousel-button").click(function() {
        if ($("#carousel-button").children("span").hasClass("fa-pause")) {
            $("#mycarousel").carousel('pause');
            $(this).children("span").removeClass("fa-pause").addClass("fa-play");
        }
        else if ($("#carousel-button").children("span").hasClass("fa-play")) {
            $("#mycarousel").carousel("cycle");
            $(this).children("span").removeClass("fa-play").addClass("fa-pause")
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
});